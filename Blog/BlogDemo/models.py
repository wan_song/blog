from django.db import models
from django.contrib.auth.models import User
from datetime import datetime
# Create your models here.
class Topic(models.Model):

    user = models.ForeignKey(User,default='1')
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=2000)
    create_time = models.DateTimeField(default=datetime.now())

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['create_time']


class Commit(models.Model):

    topic = models.ForeignKey(Topic)
    message = models.CharField(max_length=2000)
    create_time = models.DateTimeField("Time")

    def __unicode__(self):
        return self.message+"   createDate:"+str(self.create_time)

    class Meta:
        ordering = ['create_time']