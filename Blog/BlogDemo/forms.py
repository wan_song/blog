__author__ = 'Administrator'
from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput)
    password = forms.CharField(widget=forms.PasswordInput)

class RegisterForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput)
    password = forms.CharField(widget=forms.PasswordInput)
    e_mail = forms.EmailField()

class TopicForm(forms.Form):
    title = forms.CharField(widget=forms.TextInput(),min_length=10)
    content=forms.CharField(widget=forms.Textarea,min_length=20)