from forms import *
from django.contrib.auth import authenticate,login,logout
from django.shortcuts import *
from BlogDemo.models import *
# Create your views here.
def userLogin(request):
    errors = []
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
           cd = form.cleaned_data
           username = cd['username']
           password = cd['password']
           user = authenticate(username=username,password=password)
           if user is not None:
               if user.is_active:
                   login(request,user)
                   topics = Topic.objects.filter(user = user)
                   return render_to_response("user/topicList.html",locals())
               else:
                   return HttpResponse(username+" is not active!")
           else:
                form = LoginForm(initial={'username':username})
                return  HttpResponseRedirect('/blog/register',locals())
    else: form = LoginForm(initial={'username':'write your username'})
    return render_to_response("user/login.html",{'form':form})

def register(request):
    errors = []
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
           cd = form.cleaned_data
           username = cd['username']
           password = cd['password']
           email = cd['e_mail']
           user = User.objects.create_user(username, email,password)
           if username and password and user is not None:
               return HttpResponseRedirect('/blog/login')
           else:
                form = RegisterForm(initial={'username':username})
                return render_to_response("user/register.html",locals())
    else: form = RegisterForm(initial={'username':'write your username'})
    return render_to_response("user/register.html",{'form':form})


def topicList(request):
    if not request.user.is_authenticated():
            return HttpResponseRedirect('/blog/login')
    q=''
    if 'q'in request.GET:
        q=request.GET['q']
        topics=Topic.objects.filter(title__icontains=q,user=request.user)
    return render_to_response("user/topicList.html",locals())

def addTopic(request):
    if not request.user.is_authenticated():
       return HttpResponseRedirect('/blog/login')

    errors = []
    if request.method == 'POST':
        form = TopicForm(request.POST)
        if form.is_valid():
           cd = form.cleaned_data
           title = cd['title']
           content = cd['content']
           t=Topic(user=request.user,title=title,content=content)
           t.save()
           return render_to_response("user/topicList.html",{'topics':Topic.objects.filter(user=request.user,title__icontains='')})
    else:
        form = TopicForm(initial={'title':'write your title'})
    return render_to_response("user/addTopic.html",{'form':form})