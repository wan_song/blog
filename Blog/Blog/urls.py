from django.conf.urls import include, url
from django.contrib import admin
from BlogDemo import views
urlpatterns = [
    # Examples:
    # url(r'^$', 'Blog.views.home', name='home'),
    url(r'^blog/addTopic/', views.addTopic),
    url(r'^blog/admin/', include(admin.site.urls)),
    url(r'^blog/login/', views.userLogin),
    url(r'^blog/register/', views.register),
    url(r'^blog/topicList/', views.topicList),
    url(r'^blog/logout/', views.logout),
]
